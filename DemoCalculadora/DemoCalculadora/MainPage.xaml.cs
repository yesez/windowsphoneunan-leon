﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;

namespace DemoCalculadora
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();
        }

        private void btnCalcular_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                double num1 = 0, num2 = 0;
                num1 = Double.Parse(this.txtNum1.Text);
                num2 = Double.Parse(this.txtNum2.Text);

                MessageBox.Show((num1 + num2).ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ingrese 2 números por favor.");
            }
        }
    }
}