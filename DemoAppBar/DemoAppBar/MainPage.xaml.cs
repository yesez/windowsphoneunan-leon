﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;

namespace DemoAppBar
{
	public partial class MainPage : PhoneApplicationPage
	{
		// Constructor
		public MainPage()
		{
			InitializeComponent();
		}

		private void btnLogin_Click(object sender, System.EventArgs e)
		{
			// TODO: Add event handler implementation here.
			if (string.IsNullOrEmpty(this.txtNombre.Text) || string.IsNullOrEmpty(this.txtClave.Password))
			{
				MessageBox.Show("Llene todos los campos");
			}
			else
			{
                if (string.Equals(this.txtNombre.Text, "unan") && string.Equals(this.txtClave.Password, "123"))
				{
					MessageBox.Show("Bienvenido!");
					this.listaRSS.Visibility = Visibility.Visible;
                    this.panelLogin.Visibility = Visibility.Collapsed;
				}
				else
				{
					MessageBox.Show("Error!");
				}
			}
		}

		private void btnLimpiar_Click(object sender, System.EventArgs e)
		{
			// TODO: Add event handler implementation here.
			this.txtNombre.Text = string.Empty;
			this.txtClave.Password = string.Empty;
		}
	}
}