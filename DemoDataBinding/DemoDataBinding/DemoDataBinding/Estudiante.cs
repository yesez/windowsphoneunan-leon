﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace DemoDataBinding
{
    public class Estudiante
    {
        public Estudiante(String nombre, String apellido, int edad)
        {
            Nombre = nombre;
            Apellido = apellido;
            Edad = edad;
        }

        public String Nombre { get; set; }
        public String Apellido { get; set; }
        public int Edad { get; set; }
    }
}
