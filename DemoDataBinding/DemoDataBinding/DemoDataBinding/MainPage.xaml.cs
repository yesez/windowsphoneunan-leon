﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using System.Collections.ObjectModel;

namespace DemoDataBinding
{
    public partial class MainPage : PhoneApplicationPage
    {

        // Constructor
        public MainPage()
        {
            InitializeComponent();
            InicializarDatos();
            
        }

        public void InicializarDatos()
        {
            Estudiante est = new Estudiante("Juan","Alvarado",19);
            LayoutRoot.DataContext = est;
        }       
    }
}