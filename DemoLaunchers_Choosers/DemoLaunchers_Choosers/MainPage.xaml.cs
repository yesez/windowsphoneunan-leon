﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Tasks;

namespace DemoLaunchers_Choosers
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();
        }

        private void btnLauncher_Click(object sender, RoutedEventArgs e)
        {
            // Vamos a enviar un SMS
            SmsComposeTask tareaSMS = new SmsComposeTask();
            tareaSMS.To = "89905077";
            tareaSMS.Body = "Hola a todos los estudiantes de UNAN León";
            tareaSMS.Show();

            //Vamos a buscar algo en la web
            /*
            SearchTask searchTask = new SearchTask();
            searchTask.SearchQuery = "Xbox game trailers";
            searchTask.Show();
             */
        }

        private void btnChooser_Click(object sender, RoutedEventArgs e)
        {
            //Vamos a elegir una persona de la lista de contactos
            PhoneNumberChooserTask tareaContactos = new PhoneNumberChooserTask();
            tareaContactos.Completed += new EventHandler<PhoneNumberResult>(tareaContactos_Completed);
            tareaContactos.Show();
        }

        void tareaContactos_Completed(object sender, PhoneNumberResult e)
        {
            if (e != null && e.TaskResult == TaskResult.OK)
            {
                string nombreContacto = e.DisplayName;
                string numeroContacto = e.PhoneNumber;
                MessageBox.Show("El nombre es: " + nombreContacto + "\nEl Número es: " + numeroContacto);
            }
        }

        private void btnCombinar_Click(object sender, RoutedEventArgs e)
        {
            PhoneNumberChooserTask tareaContactos = new PhoneNumberChooserTask();
            tareaContactos.Completed += new EventHandler<PhoneNumberResult>(tareaContactosII_Completed);
            tareaContactos.Show();
        }

        void tareaContactosII_Completed(object sender, PhoneNumberResult e)
        {
            if (e != null && e.TaskResult == TaskResult.OK)
            {
                string nombreContacto = e.DisplayName;
                string numeroContacto = e.PhoneNumber;
                
                SmsComposeTask tareaSMS = new SmsComposeTask();
                tareaSMS.To = numeroContacto;
                tareaSMS.Body = "Hola " + nombreContacto + "!";
                tareaSMS.Show();
            }
        }
    }
}