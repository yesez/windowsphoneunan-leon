﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;

namespace MetroWiki
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();
            this.DataContext = App.ViewModel;
        }

        private void txtConsulta_TextChanged(object sender, TextChangedEventArgs e)
        {   
            var binding = this.txtConsulta.GetBindingExpression(TextBox.TextProperty);
            binding.UpdateSource();            
        }

        private void btnFavs_Click(object sender, System.EventArgs e)
        {
            App.ViewModel.cargarFavoritos();
        }
    }
}