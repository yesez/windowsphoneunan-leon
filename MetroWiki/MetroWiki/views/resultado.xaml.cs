﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using MetroWiki.lib;

namespace MetroWiki.views
{
    public partial class resultado : PhoneApplicationPage
    {
        public resultado()
        {
            InitializeComponent();
            this.DataContext = App.ResultViewModel;
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            App.ResultViewModel.ElementoSeleccionado = App.ViewModel.ElementoSeleccionado;
            base.OnNavigatedTo(e);
        }

        private void btnCompartir_Click(object sender, System.EventArgs e)
        {
            SmsComposeTask tareaSMS = new SmsComposeTask();
            tareaSMS.Body = "Hola, Te invito a leer: '" + App.ResultViewModel.ElementoSeleccionado.Texto +  "' en " + 
                App.ResultViewModel.ElementoSeleccionado.Url;
            tareaSMS.Show();
        }

        private void btnGuardar_Click(object sender, System.EventArgs e)
        {
            if (App.ResultViewModel.guardarEnFavoritos() == true)
                MessageBox.Show("Favorito Guardado");
            else
                MessageBox.Show("Error al guardar :(");
        }
        
    }
}