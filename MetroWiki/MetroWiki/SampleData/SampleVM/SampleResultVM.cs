﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using MetroWiki.viewmodels;
using MetroWiki.models;

namespace MetroWiki.SampleData.SampleVM
{
    public class SampleResultVM : BaseViewModel
    {
        private Model _ElementoSeleccionado;
        public Model ElementoSeleccionado
        {
            get
            {
                if (DesignerProperties.IsInDesignTool)
                {
                    _ElementoSeleccionado = new Model
                    {
                        Texto = "Microsoft Windows",
                        Descripcion = "Descripción",
                        Url = "http://es.wikipedia.org/"
                    };
                }
                return _ElementoSeleccionado;
            }
            set
            {
                if (_ElementoSeleccionado != value)
                {
                    _ElementoSeleccionado = value;
                    OnPropertyChanged(() => this.ElementoSeleccionado);
                }
            }
        }
    }
}
