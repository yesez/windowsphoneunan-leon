﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.IO.IsolatedStorage;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace MetroWiki.lib
{
	public class ISHelper
	{       

		public ISHelper()
		{           
		}

		public static bool guardarPorValor(string llave, Object valor)
		{
			IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
			bool respuesta = false;

			if (settings.Contains(llave))
			{
				if (settings[llave] != valor)
				{
					settings[llave] = valor;                    
				}
				respuesta = true;
			}
			else
			{
				settings.Add(llave, valor);
				respuesta = true;
			}
			
			return respuesta;
		}

		public static object obtenerPorValor(string llave)
		{
			IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
			object valor = null;

			if (settings.Contains(llave))
			{
				valor = settings[llave];
			}

			return valor;
		}

		public static bool siExiste(string llave)
		{
			IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
			if (settings.Contains(llave)) return true; else return false;
		}

		public static bool crearDirectorio(string nombreDirectorio)
		{
			bool respuesta = false;

			try
			{
				using (IsolatedStorageFile almacAislado = IsolatedStorageFile.GetUserStoreForApplication())
				{
					if (!string.IsNullOrEmpty(nombreDirectorio) && !almacAislado.DirectoryExists(nombreDirectorio))
					{
						almacAislado.CreateDirectory(nombreDirectorio);
						if (almacAislado.DirectoryExists(nombreDirectorio) == true)
							respuesta = true;
					}
				}
			}
			catch (Exception){              
			}

			return respuesta;
		}

		public static bool existeDirectorio(string nombreDirectorio)
		{
			bool respuesta = false;
			try
			{
				using (IsolatedStorageFile almacAislado = IsolatedStorageFile.GetUserStoreForApplication())
				{
					if (almacAislado.DirectoryExists(nombreDirectorio) == true)
						respuesta = true;
				}
			}
			catch (Exception)
			{ }

			return respuesta;
		}

		public static bool existeArchivo(string nombreDirectorio, string nombreArchivo)
		{
			bool respuesta = false;
			try
			{
				using (IsolatedStorageFile almacAislado = IsolatedStorageFile.GetUserStoreForApplication())
				{
					string rutaArchivo = obtenerRutaArchivo(nombreDirectorio, nombreArchivo);
					if (almacAislado.FileExists(rutaArchivo) == true)
						respuesta = true;
				}
			}
			catch (Exception)
			{ }

			return respuesta;
		}      

		public static bool eliminarDirectorio(string nombreDirectorio)
		{
			bool respuesta = false;

			try
			{
				using (IsolatedStorageFile almacAislado = IsolatedStorageFile.GetUserStoreForApplication())
				{
					if (!string.IsNullOrEmpty(nombreDirectorio) && almacAislado.DirectoryExists(nombreDirectorio))
					{
						almacAislado.DeleteDirectory(nombreDirectorio);
						if(almacAislado.DirectoryExists(nombreDirectorio) == false)
							return true;
					}
				}
			}
			catch (Exception){
			}

			return respuesta;
		}

		public static bool crearArchivo(string nombreDirectorio, string nombreArchivo, string contenido, bool esNuevo)
		{
			bool respuesta = false;

			try
			{
				using (IsolatedStorageFile almacAislado = IsolatedStorageFile.GetUserStoreForApplication())
				{
					if (string.IsNullOrEmpty(nombreArchivo) == false)
					{

						string rutaArchivo = obtenerRutaArchivo(nombreDirectorio, nombreArchivo);

						if (esNuevo)
						{
							if (almacAislado.FileExists(rutaArchivo))
							{
								almacAislado.DeleteFile(rutaArchivo);
							}
						}

						StreamWriter archivoDestino = new StreamWriter(new IsolatedStorageFileStream(rutaArchivo, FileMode.OpenOrCreate, almacAislado));

						archivoDestino.WriteLine(contenido);
						archivoDestino.Close();
						respuesta = true;
					}
				}
			}
			catch (Exception){                
			}

			return respuesta;
		}

		public static bool crearArchivo<T>(string nombreDirectorio, string nombreArchivo, T clase, bool esNuevo)
		{
			bool respuesta = false;

			try
			{
				using (IsolatedStorageFile almacAislado = IsolatedStorageFile.GetUserStoreForApplication())
				{
					if (string.IsNullOrEmpty(nombreArchivo) == false)
					{

						string rutaArchivo = obtenerRutaArchivo(nombreDirectorio, nombreArchivo);
						XmlWriterSettings configEscritor = new XmlWriterSettings();
						configEscritor.Indent = true;

						if (esNuevo)
						{
							if (almacAislado.FileExists(rutaArchivo))
							{
								almacAislado.DeleteFile(rutaArchivo);
							}
						}

						using (IsolatedStorageFileStream stream = almacAislado.OpenFile(rutaArchivo, FileMode.OpenOrCreate))
						{
							XmlSerializer serializador = new XmlSerializer(typeof(T));
							using (XmlWriter escritor = XmlWriter.Create(stream, configEscritor))
							{
								serializador.Serialize(escritor, clase);
							}
						}
					   
						respuesta = true;
					}
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message.ToString());
			}

			return respuesta;
		}


		public static string leerArchivo(string nombreDirectorio, string nombreArchivo)
		{
			string resultado = null;

			try
			{
				using (IsolatedStorageFile almacAislado = IsolatedStorageFile.GetUserStoreForApplication())
				{
					if (!string.IsNullOrEmpty(nombreArchivo))
					{                        
						StreamReader lector = new StreamReader(new IsolatedStorageFileStream(
							obtenerRutaArchivo(nombreDirectorio, nombreArchivo)
							, FileMode.Open
							, almacAislado));

						resultado = lector.ReadLine();

						lector.Close();
					}
				}
			}
			catch (Exception){               
			}

			return resultado;
		}

		public static T leerArchivo<T>(string nombreDirectorio, string nombreArchivo)
		{
			T resultado = default(T);

			try
			{
				using (IsolatedStorageFile almacAislado = IsolatedStorageFile.GetUserStoreForApplication())
				{
					if (!string.IsNullOrEmpty(nombreArchivo))
					{
						using (IsolatedStorageFileStream stream = almacAislado.OpenFile(
							obtenerRutaArchivo(nombreDirectorio, nombreArchivo), 
							FileMode.Open))
						{
							XmlSerializer serializer = new XmlSerializer(typeof(T));
							resultado = (T)serializer.Deserialize(stream);                            
						}
					}
				}
			}
			catch (Exception)
			{
			}

			return resultado;
		}

		public static bool borrarArchivo(string nombreDirectorio, string nombreArchivo)
		{
			bool respuesta = false;

			try
			{
				using (IsolatedStorageFile almacAislado = IsolatedStorageFile.GetUserStoreForApplication())
				{
					if (!string.IsNullOrEmpty(nombreArchivo))
					{
						string ruta = obtenerRutaArchivo(nombreDirectorio, nombreArchivo);
						almacAislado.DeleteFile(ruta);
						if (almacAislado.FileExists(ruta) == false)
							respuesta = true;
					}
				}
			}
			catch (Exception){
			}

			return respuesta;
		}

		public static string obtenerRutaArchivo(string nombreDirectorio, string nombreArchivo) //ej Carpeta, prueba.txt
		{
			if (nombreDirectorio != null && !nombreDirectorio.EndsWith("\\"))
				nombreDirectorio += "\\";

			return (nombreDirectorio + nombreArchivo);
		}

		public static string[] obtenerArchivosPorDir(string nombreDirectorio)
		{
			string[] nombreArchivos = default(string[]);
			
				using (IsolatedStorageFile almacAislado = IsolatedStorageFile.GetUserStoreForApplication())
				{
					if(almacAislado.DirectoryExists(nombreDirectorio))
						nombreArchivos = almacAislado.GetFileNames(nombreDirectorio + "\\*.*");
				}

			return nombreArchivos;
		}

	}
}
