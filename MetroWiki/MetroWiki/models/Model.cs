﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MetroWiki.models
{
    public class Model
    {
        public string Texto { get; set; }
        public string Descripcion { get; set; }
        public string Url { get; set; }        
    }
}
