﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Linq.Expressions;

namespace MetroWiki.viewmodels
{    
        public class BaseViewModel : INotifyPropertyChanged
        {
            public event PropertyChangedEventHandler PropertyChanged;

            protected virtual void OnPropertyChanged(string propertyName)
            {
                var handler = PropertyChanged;
                if (handler != null)
                {                    
                    handler(this, new PropertyChangedEventArgs(propertyName));
                }
            }

            protected virtual void OnPropertyChanged<TViewModel>(Expression<Func<TViewModel>> property)
            {
                var expression = property.Body as MemberExpression;
                var member = expression.Member;

                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(member.Name));
                }
            }
        }    
}
