﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using MetroWiki.models;
using System.ComponentModel;
using MetroWiki.lib;
using System.Collections.Generic;

namespace MetroWiki.viewmodels
{
	public class ResultViewModel: BaseViewModel
	{
		private Model _ElementoSeleccionado;
		public Model ElementoSeleccionado
		{
			get {                
				return _ElementoSeleccionado; 
			}
			set {
				if (_ElementoSeleccionado != value)
				{
					_ElementoSeleccionado = value;
					OnPropertyChanged(() => this.ElementoSeleccionado);
				}
			}
		}          
	
		public ResultViewModel()
		{            
			
		}	
	
		public bool guardarEnFavoritos()
		{
			bool resp = true;
			List<Model> listaFavs = null;
			if (ISHelper.siExiste(App.llaveFavs) == true)
			{
				listaFavs = (List<Model>)ISHelper.obtenerPorValor(App.llaveFavs);                
			}
			else
			{
				listaFavs = new List<Model>();
			}
			
			listaFavs.Add(ElementoSeleccionado);
			resp = ISHelper.guardarPorValor(App.llaveFavs, listaFavs);
			return resp;
		}
	}
}
