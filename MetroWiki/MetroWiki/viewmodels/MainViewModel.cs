﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Linq;
using System.Xml.Linq;
using System.Collections.Generic;
using MetroWiki.lib;
using MetroWiki.models;
using Microsoft.Phone.Shell;

namespace MetroWiki.viewmodels
{
    public class MainViewModel:BaseViewModel
    {
        private string _Consulta = "UNAN";
        public string Consulta
        {
            get {
                return _Consulta; 
            }
            set
            {
                if (_Consulta != value)
                {
                    _Consulta = value;
                    OnPropertyChanged(() => this.Consulta);
                }                

                if (string.IsNullOrEmpty(_Consulta))
                {
                    (ComandoBuscar as RelayCommand).IsEnabled = false;
                }
                else
                {
                    (ComandoBuscar as RelayCommand).IsEnabled = true;
                }
            }
        }

        private readonly ICommand _ComandoBuscar;
        public ICommand ComandoBuscar
        {
            get { return _ComandoBuscar; }
        }
                
        public string Fuente { get; set; }
        
        private List<Model> _Resultados;
        public List<Model> Resultados
        {
            get { return _Resultados; }
            set
            {
                if (_Resultados != value)
                {
                    _Resultados = value;
                    OnPropertyChanged(() => this.Resultados);
                }
            }
        }

        private Model _ElementoSeleccionado;
        public Model ElementoSeleccionado
        {
            get { return _ElementoSeleccionado; }
            set
            {
                if (_ElementoSeleccionado != value)
                {
                    _ElementoSeleccionado = value;
                    OnPropertyChanged(() => this.ElementoSeleccionado);
                    this.cargarElementoSeleccionado();
                }
            }
        }

        private bool _Cargando;
        public bool Cargando
        {
            get { return _Cargando; }
            set
            {
                if (_Cargando != value)
                {
                    _Cargando = value;
                    OnPropertyChanged(() => this.Cargando);
                }
            }
        }     

        public MainViewModel()
        {
            _ComandoBuscar = new RelayCommand(buscar) { IsEnabled=false };            
        }
        
        private void buscar()
        {
            if (GeneralHelper.existeConexion())
            {
                this.Cargando = true;
                this.Fuente = string.Format("http://es.wikipedia.org/w/api.php?action=opensearch&format=xml&search={0}",
                              Consulta);

                WebClient proxy = new WebClient();
                proxy.DownloadStringCompleted += new DownloadStringCompletedEventHandler(proxy_DownloadStringCompleted);
                proxy.DownloadStringAsync(new Uri(this.Fuente));    
            }
            else
            {
                MessageBox.Show("No podemos conectarnos en estos momentos :(");
            }
        }

        void proxy_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                procesarDatos(e.Result);
                this.Cargando = false;
            }
            else
            {
                MessageBox.Show("Error! :(");
            }
        }

        private void procesarDatos(string e)
        {
            XDocument doc = XDocument.Parse(e);
            XNamespace ns = "http://opensearch.org/searchsuggest2";

            this.Resultados = (from elemento in doc.Descendants(ns + "Item")
                               select new Model
                               {
                                   Descripcion = elemento.Element(ns + "Description").Value,
                                   Texto = elemento.Element(ns + "Text").Value,
                                   Url = elemento.Element(ns + "Url").Value
                               }
                              ).ToList();
            
            if (this.Resultados.Count < 1)
            {
                MessageBox.Show("No se encontraron elementos :(");
            }
        }

        private void cargarElementoSeleccionado()
        {            
            if (this.ElementoSeleccionado != null)
            {
                string ruta = string.Format("/views/resultado.xaml?{0}", this.ElementoSeleccionado.Url);
                var rootFrame = (App.Current as App).RootFrame;                
                rootFrame.Navigate(new System.Uri(ruta, System.UriKind.Relative));
            }
        }

        public void cargarFavoritos()
        {
            if (ISHelper.siExiste(App.llaveFavs) == true)
            { 
                this.Resultados = (List<Model>)ISHelper.obtenerPorValor(App.llaveFavs);                
            }
            else
            {
                MessageBox.Show("No se encontraron favoritos :/");
            }            
        }

    }
}
